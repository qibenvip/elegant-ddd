package com.elegantboot.framework.jpa.config;

import com.querydsl.jpa.impl.JPAQueryFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

import javax.persistence.EntityManager;

@Configuration
@EnableJpaAuditing
public class ElegantBootJPAConfiguration {

    @Bean
    @ConditionalOnMissingBean
    public JPAQueryFactory getJPAQueryFactory(EntityManager entityManager) {
        return new JPAQueryFactory(entityManager);
    }

}
