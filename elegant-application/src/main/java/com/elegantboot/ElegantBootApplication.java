package com.elegantboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ElegantBootApplication {

    public static void main(String[] args) {
        SpringApplication.run(ElegantBootApplication.class, args);
    }

}
